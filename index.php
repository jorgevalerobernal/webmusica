<?php
require ('classes/disco.class.php');
require ('classes/coleccion.class.php');

if(isset($_POST['enviar'])){
  $nombre=$_POST['nombre'];
  $año=$_POST['año'];
  $autor=$_POST['autor'];
  $canciones=$_POST['canciones'];

  

  $linea="\r\n".$nombre.';'.$año.';'.$autor.';'.$canciones.','.$portada;
  $fichero=fopen('datos.txt','a');

  fwrite($fichero,$linea);

  fclose($fichero);
}

$coleccion=new Coleccion('Coleccion de Discos');
$fichero=fopen('datos.txt','r');
$linea=fgets($fichero);

while ($linea=fgets($fichero)) {
  $partes=explode(';',$linea);
  $nombre=$partes[0];
  $año=$partes[1];
  $autor=$partes[2];
  $canciones=$partes[3];
  $portada=$partes[4];
  
  $coleccion->agregar(new Disco($nombre,$año,$autor,$canciones,$portada));
}

fclose($fichero);

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plantilla básica de Bootstrap</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <section class="container"> 

      <header>
        <h2>Coleccion de discos
          <small><?php echo $coleccion->titulo; ?></small></h2>
      </header>
                <section class="row">
                <div class="col-md-8">
                 <table border="1" class="table table-hover table-striped">
                   <tr>
                     <th>Nombre</th>
                     <th>Año</th>
                     <th>Autor</th>
                     <th>Nº de Canciones</th>
                     <th>Portada</th>
                     <th>Acciones</th>
                   </tr>
                   <?php foreach($coleccion->listar() as $disco){ ?>

      <tr>
        <td><?php echo $disco->nombre; ?></td>
        <td><?php echo $disco->año; ?></td>
        <td><?php echo $disco->autor; ?></td>
        <td><?php echo $disco->canciones ?></td>
        <td><img src="imagenes/<?php echo $disco->portada;?>" width="100"></td>
        <td>Ver - Borrar - Modificar</td>
      </tr>
    <?php } ?>
    

    </table>
  </div>

                
        
        <div class="col-md-4">
        <form action="index.php" method="post" enctype="multipart/form-data">
          <input type="text" class="form-control" name="nombre" placeholder="Escribe el nombre del disco">
          <input type="text" class="form-control" name="año" placeholder="Escribe el año del disco">
          <input type="text" class="form-control" name="autor" placeholder="Escribe el autor del disco">
          <input type="text" class="form-control" name="canciones" placeholder="Escribe numero de canciones del disco">
          <input type="file" class="form-control" name="portada">
          <input type="submit" name="enviar" value="enviar">
        </form>
      </div>
    </section>
    <footer>
        <hr>
        Todos los derechos reservados &copy;
      </footer>

    </section>
    
     

 
    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap -->
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>